import { Component } from '@angular/core';

@Component({
  selector: 'g-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'glyb-components';
}
